package com.example.pokedex.details.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pokedex.R

class PokemonDetailsAdapter(details: List<PokemonDetailsItem>) :
    RecyclerView.Adapter<PokemonDetailsViewHolder>() {

  private var currentList = details

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonDetailsViewHolder {
    val layout: View? = when (viewType) {
      ItemType.LIST.ordinal -> {
        LayoutInflater.from(parent.context)
            .inflate(R.layout.pokemon_details_list_item, parent, false)
      }
      ItemType.SINGLE.ordinal -> {
        LayoutInflater.from(parent.context)
            .inflate(R.layout.pokemon_details_single_item, parent, false)
      }
      else -> null
    }
    return PokemonDetailsViewHolder(layout!!)
  }

  override fun onBindViewHolder(holder: PokemonDetailsViewHolder, position: Int) {
    holder.bind(currentList[position])
  }

  override fun getItemViewType(position: Int): Int {
    return currentList[position].itemType.ordinal
  }

  override fun getItemCount() = currentList.size

  fun setList(pokemonItemList: List<PokemonDetailsItem>) {
    currentList = pokemonItemList
    notifyDataSetChanged()
  }
}
