package com.example.pokedex.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import com.bumptech.glide.Glide
import com.example.pokedex.R
import com.example.pokedex.details.adapter.PokemonDetailsAdapter
import com.example.pokedex.details.adapter.PokemonDetailsItem
import com.example.pokedex.repositories.PokemonItem
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_pokemon_details.*
import javax.inject.Inject

@AndroidEntryPoint
class PokemonDetailsFragment : Fragment(), PokemonDetailsView {

  @Inject
  lateinit var viewModel: PokemonDetailsViewModel
  private lateinit var adapter: PokemonDetailsAdapter

  companion object {

    const val POKEMON_ITEM_KEY = "pokemon_item_key"

    @JvmStatic
    fun newInstance(pokemonItem: PokemonItem): PokemonDetailsFragment {
      val fragment = PokemonDetailsFragment()
      fragment.apply {
        arguments = Bundle().apply {
          putSerializable(POKEMON_ITEM_KEY, pokemonItem)
        }
      }
      return fragment
    }
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                            savedInstanceState: Bundle?): View? {
    return inflater.inflate(R.layout.fragment_pokemon_details, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    adapter = PokemonDetailsAdapter(emptyList())
    details_recycler.adapter = adapter
    setupViewModel()
  }

  private fun setupViewModel() {
    setupOnViewData()
    setupOnPokemonItem()
  }

  private fun setupOnViewData() {
    viewModel.viewDataModel
        .observe(viewLifecycleOwner) {
          setupToolbarTitle(it.name)
          setupImage(it.image)
        }
  }

  private fun setupOnPokemonItem() {
    viewModel.detailsModel.observe(viewLifecycleOwner) {
      if (it.error == null) setupDetails(it.itemsList)
      else showError()
    }
  }

  override fun setupImage(image: String) {
    Glide.with(requireContext())
        .load(image)
        .circleCrop()
        .into(pokemon_details_image)
  }

  override fun showError() {
    Toast.makeText(context, R.string.error_info, Toast.LENGTH_LONG)
        .show()
  }

  override fun setupToolbarTitle(name: String) {
    activity?.title = name
  }

  override fun setupDetails(pokemonItemList: List<PokemonDetailsItem>) {
    adapter.setList(pokemonItemList)
  }
}
