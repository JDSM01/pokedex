package com.example.pokedex.details.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pokedex.repositories.AbilityModel
import com.example.pokedex.R

class PokemonDetailsListAdapter(detailsList: List<AbilityModel>) :
    RecyclerView.Adapter<PokemonDetailsListViewHolder>() {

  private val currentList = detailsList
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonDetailsListViewHolder {
    val layout = LayoutInflater.from(parent.context)
        .inflate(R.layout.pokemon_details_list_item_item, parent, false)
    return PokemonDetailsListViewHolder(layout)
  }

  override fun onBindViewHolder(holder: PokemonDetailsListViewHolder, position: Int) {
    holder.bind(currentList[position])
  }

  override fun getItemCount() = currentList.size

}
