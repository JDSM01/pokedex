package com.example.pokedex.details.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.pokedex.repositories.AbilityModel
import kotlinx.android.synthetic.main.pokemon_details_list_item_item.view.*

class PokemonDetailsListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

  fun bind(abilityModel: AbilityModel) {
    itemView.name.text = abilityModel.name
    itemView.hidden.visibility = if (abilityModel.isHidden) View.VISIBLE else View.GONE
  }
}
