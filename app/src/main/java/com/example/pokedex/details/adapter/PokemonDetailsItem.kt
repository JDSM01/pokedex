package com.example.pokedex.details.adapter

import java.io.Serializable

data class PokemonDetailsItem(val itemType: ItemType, val detailsType: DetailsType, val item: Any) :
    Serializable

enum class ItemType {
    LIST, SINGLE
}

enum class DetailsType {
    ABILITY, BASE_EXPERIENCE, HEIGHT, WEIGHT, NUMBER, TYPE
}
