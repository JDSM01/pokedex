package com.example.pokedex.details

import com.example.pokedex.details.adapter.PokemonDetailsItem

interface PokemonDetailsView {

  fun setupDetails(pokemonItemList: List<PokemonDetailsItem>)

  fun setupImage(image: String)

  fun showError()

  fun setupToolbarTitle(name: String)
}
