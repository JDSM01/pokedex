package com.example.pokedex.details

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pokedex.repositories.PokemonItem
import com.example.pokedex.repositories.PokemonRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
class PokemonDetailsModule {

  @Provides
  fun providesPokemonDetailsPresenter(fragment: Fragment,
                                      factory: PokemonDetailsViewModelFactory): PokemonDetailsViewModel {
    return ViewModelProvider(fragment, factory).get(PokemonDetailsViewModel::class.java)
  }

  @Provides
  fun providesPokemonDetailsData(fragment: Fragment): PokemonDetailsData {
    fragment.requireArguments()
        .apply {
          return PokemonDetailsData(
              getSerializable(PokemonDetailsFragment.POKEMON_ITEM_KEY) as PokemonItem)
        }
  }

  @Provides
  fun providesPokemonDetailsInteractor(repository: PokemonRepository): PokemonDetailsInteractor {
    return PokemonDetailsInteractor(repository)
  }

  @Provides
  fun providesPokemonDetailsViewModelFactory(data: PokemonDetailsData,
                                             interactor: PokemonDetailsInteractor): PokemonDetailsViewModelFactory {
    return PokemonDetailsViewModelFactory(data, interactor)
  }
}

class PokemonDetailsViewModelFactory(private val data: PokemonDetailsData,
                                     private val interactor: PokemonDetailsInteractor) :
    ViewModelProvider.Factory {
  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(PokemonDetailsViewModel::class.java)) {
      return PokemonDetailsViewModel(interactor, data) as T
    }
    throw IllegalArgumentException("Unknown ViewModel class")
  }
}