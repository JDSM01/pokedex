package com.example.pokedex.details.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.pokedex.repositories.AbilityModel
import com.example.pokedex.R
import kotlinx.android.synthetic.main.pokemon_details_list_item.view.*

class PokemonDetailsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

  fun bind(item: PokemonDetailsItem) {
    itemView.title.text = getStringFromType(item.detailsType, item.item)
    if (item.itemType == ItemType.LIST) {
      if (item.detailsType == DetailsType.ABILITY) {
        itemView.details_list.adapter = PokemonDetailsListAdapter(item.item as List<AbilityModel>)
      } else if (item.detailsType == DetailsType.TYPE) {
        val typeList: MutableList<AbilityModel> = ArrayList()
        (item.item as List<String>).forEach { typeList.add(AbilityModel(it, false)) }
        itemView.details_list.adapter = PokemonDetailsListAdapter(typeList)
      }
    }
  }

  private fun getStringFromType(detailsType: DetailsType, item: Any): String {
    val context = itemView.context
    return when (detailsType) {
      DetailsType.ABILITY -> context.getString(R.string.abilities)
      DetailsType.TYPE -> context.getString(R.string.types)
      DetailsType.BASE_EXPERIENCE -> context.getString(R.string.base_experience, item.toString())
      DetailsType.WEIGHT -> context.getString(R.string.weight, item.toString())
      DetailsType.HEIGHT -> context.getString(R.string.height, item.toString())
      DetailsType.NUMBER -> context.getString(R.string.number, item.toString())
    }
  }

}
