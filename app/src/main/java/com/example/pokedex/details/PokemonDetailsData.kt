package com.example.pokedex.details

import com.example.pokedex.repositories.PokemonItem

data class PokemonDetailsData(val pokemonItem: PokemonItem)
