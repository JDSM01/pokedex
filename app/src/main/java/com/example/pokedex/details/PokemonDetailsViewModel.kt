package com.example.pokedex.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokedex.details.adapter.PokemonDetailsItemModel
import com.example.pokedex.repositories.PokemonItem
import kotlinx.coroutines.launch

class PokemonDetailsViewModel(private val interactor: PokemonDetailsInteractor,
                              private val data: PokemonDetailsData) : ViewModel() {

  val detailsModel: MutableLiveData<PokemonDetailsItemModel> = MutableLiveData()
  val viewDataModel: MutableLiveData<PokemonItem> = MutableLiveData()

  init {
    viewDataModel.postValue(data.pokemonItem)
    handleDetails()
  }

  private fun handleDetails() {
    viewModelScope.launch {
      val pokemon = interactor.retrievePokemon(data.pokemonItem.number)
      detailsModel.postValue(pokemon)
    }
  }
}
