package com.example.pokedex.details

import com.example.pokedex.details.adapter.DetailsType
import com.example.pokedex.details.adapter.ItemType
import com.example.pokedex.details.adapter.PokemonDetailsItem
import com.example.pokedex.details.adapter.PokemonDetailsItemModel
import com.example.pokedex.repositories.PokemonModel
import com.example.pokedex.repositories.PokemonRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PokemonDetailsInteractor(private val pokemonRepository: PokemonRepository) {

  suspend fun retrievePokemon(id: Int): PokemonDetailsItemModel {
    return withContext(Dispatchers.IO) {
      val model = pokemonRepository.retrievePokemon(id)
      mapToItem(id, model)
    }
  }

  private fun mapToItem(id: Int, pokemonModel: PokemonModel): PokemonDetailsItemModel {
    val list: MutableList<PokemonDetailsItem> = ArrayList()
    if (pokemonModel.error == null) {
      list.add(PokemonDetailsItem(ItemType.SINGLE, DetailsType.NUMBER, id))
      list.add(PokemonDetailsItem(ItemType.LIST, DetailsType.ABILITY, pokemonModel.abilities))
      list.add(PokemonDetailsItem(ItemType.LIST, DetailsType.TYPE, pokemonModel.types))
      list.add(
          PokemonDetailsItem(
              ItemType.SINGLE, DetailsType.BASE_EXPERIENCE,
              pokemonModel.baseExperience
          )
      )
      list.add(PokemonDetailsItem(ItemType.SINGLE, DetailsType.HEIGHT, pokemonModel.height))
      list.add(PokemonDetailsItem(ItemType.SINGLE, DetailsType.WEIGHT, pokemonModel.weight))
    }
    return PokemonDetailsItemModel(list, pokemonModel.error)
  }
}