package com.example.pokedex.details.adapter

import java.io.Serializable

data class PokemonDetailsItemModel(val itemsList: MutableList<PokemonDetailsItem>,
                                   val error: String?) : Serializable
