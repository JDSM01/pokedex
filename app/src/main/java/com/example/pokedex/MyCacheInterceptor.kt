package com.example.pokedex

import okhttp3.Interceptor
import okhttp3.Response

class MyCacheInterceptor : Interceptor {
  override fun intercept(chain: Interceptor.Chain): Response {
    val request = chain.request()
    request.newBuilder()
        .header("Cache-Control", "public, max-age=" + 60 * 15)
        .build()
    return chain.proceed(request)
  }

}
