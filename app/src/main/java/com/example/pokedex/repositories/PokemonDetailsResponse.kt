package com.example.pokedex.repositories

import com.google.gson.annotations.SerializedName

data class PokemonDetailsResponse(val abilities: List<AbilityResponse>?,
                                  @SerializedName("base_experience") val baseExperience: String,
                                  val height: Int,
                                  val types: List<TypeResponse>,
                                  val weight: Int
)

data class TypeResponse(val type: Type)

data class Type(val name: String, val url: String)

data class AbilityResponse(val ability: Ability, @SerializedName("is_hidden") val isHidden: Boolean,
                           val slot: String)

data class Ability(val name: String)
