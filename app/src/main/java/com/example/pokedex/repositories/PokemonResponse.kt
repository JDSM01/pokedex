package com.example.pokedex.repositories

data class PokemonResponse(val name: String, val url: String)
