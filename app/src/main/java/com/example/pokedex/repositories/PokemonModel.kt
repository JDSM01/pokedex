package com.example.pokedex.repositories

data class PokemonModel(val abilities: List<AbilityModel>, val baseExperience: String,
                        val height: Int, val weight: Int, val types: List<String>,
                        val error: String? = null) {

  constructor(error: String?) : this(emptyList(), "", 0, 0, emptyList(), error)
}

data class AbilityModel(val name: String?, val isHidden: Boolean)
