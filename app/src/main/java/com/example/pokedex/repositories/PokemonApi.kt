package com.example.pokedex.repositories

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonApi {

  @GET("pokemon-species")
  suspend fun retrieveList(@Query("offset") offset: Int = 0,
                           @Query("limit") limit: Int = 20): PokemonListResponse

  @GET("pokemon/{id}")
  suspend fun retrievePokemon(@Path("id") id: Int): PokemonDetailsResponse
}
