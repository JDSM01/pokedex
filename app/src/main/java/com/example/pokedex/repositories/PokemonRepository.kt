package com.example.pokedex.repositories

class PokemonRepository(private val api: PokemonApi) {

  suspend fun retrieveList(offset: Int): PokemonListModel {
    return try {
      val response = api.retrieveList(offset = offset)
      mapResponse(response, offset)
    } catch (e: Throwable) {
      PokemonListModel(e.message)
    }
  }

  suspend fun retrievePokemon(id: Int): PokemonModel {
    return try {
      val response = api.retrievePokemon(id)
      mapResponse(response)
    } catch (e: Throwable) {
      PokemonModel(e.message)
    }
  }

  private fun mapResponse(response: PokemonListResponse, offset: Int): PokemonListModel {
    val pokemonList: MutableList<PokemonItem> = ArrayList()
    response.results.forEachIndexed { index: Int, pokemon: PokemonResponse ->
      val pokemonNumber = offset + index + 1
      val pokemonImageUrl = IMAGE_URL + pokemonNumber + IMAGE_EXTENSION
      val pokemonModel =
          PokemonItem(pokemonNumber, pokemon.name, pokemonImageUrl)
      pokemonList.add(pokemonModel)
    }
    return PokemonListModel(pokemonList)
  }

  private fun mapResponse(response: PokemonDetailsResponse): PokemonModel {
    val abilities: MutableList<AbilityModel> = ArrayList()
    val types: MutableList<String> = ArrayList()
    response.abilities?.forEach {
      abilities.add(AbilityModel(it.ability.name, it.isHidden))
    }
    response.types.forEach { types.add(it.type.name) }
    return PokemonModel(abilities, response.baseExperience, response.height, response.weight, types)
  }


  companion object {
    private const val IMAGE_URL =
        "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/"
    private const val IMAGE_EXTENSION = ".png"
  }
}
