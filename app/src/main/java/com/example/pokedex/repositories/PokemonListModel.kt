package com.example.pokedex.repositories

import java.io.Serializable

data class PokemonListModel(val pokemonList: List<PokemonItem>, val error: String? = null) :
    Serializable {

  constructor(error: String?) : this(emptyList(), error)
}

data class PokemonItem(val number: Int, val name: String, val image: String,
                       val isLoading: Boolean = false) : Serializable {

  constructor(isLoading: Boolean) : this(0, "name", "image", isLoading)
}