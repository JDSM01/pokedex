package com.example.pokedex.entry

import com.example.pokedex.repositories.PokemonItem

interface PokemonListView {

  fun showList(pokemonList: List<PokemonItem>)

  fun showError()
}
