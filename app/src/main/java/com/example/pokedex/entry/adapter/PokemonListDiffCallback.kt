package com.example.pokedex.entry.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.pokedex.repositories.PokemonItem
import java.util.*

class PokemonListDiffCallback(private val oldList: ArrayList<PokemonItem>,
                              private val newList: List<PokemonItem>) :
    DiffUtil.Callback() {

  override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
    return oldList[oldItemPosition].number == newList[newItemPosition].number
  }

  override fun getOldListSize(): Int {
    return oldList.size
  }

  override fun getNewListSize(): Int {
    return newList.size
  }

  override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
    return oldList[oldItemPosition] == newList[newItemPosition]
  }

  override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
    return newList[newItemPosition]
  }
}
