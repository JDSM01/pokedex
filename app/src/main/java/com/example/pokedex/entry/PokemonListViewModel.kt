package com.example.pokedex.entry

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokedex.repositories.PokemonListModel
import kotlinx.coroutines.launch

class PokemonListViewModel(private val interactor: PokemonListInteractor) : ViewModel() {

  val listViewModel: MutableLiveData<PokemonListModel> = MutableLiveData()
  val showLoadMore: MutableLiveData<Boolean> = MutableLiveData()
  private val lastRequestNumber: Int = -1

  init {
    handleListRequest()
  }

  fun handleEndOfList(lastItemNumber: Int) {
    if (lastItemNumber > lastRequestNumber) {
      viewModelScope.launch {
        val isShowing = showLoadMore.value
        showLoadMore.postValue(true)
        if (isShowing == null || !isShowing) {
          val model = interactor.retrievePokemonList(lastItemNumber)
          updateViewModel(model)
        }
      }
    }
  }

  private fun updateViewModel(model: PokemonListModel) {
    if (model.error != null) listViewModel.postValue(model)
    else {
      val previousModel = listViewModel.value
      val list = previousModel!!.pokemonList.toMutableList()
      list.addAll(model.pokemonList)
      listViewModel.postValue(PokemonListModel(list))
      showLoadMore.postValue(false)
    }
  }

  private fun handleListRequest() {
    viewModelScope.launch {
      val model = interactor.retrievePokemonList()
      listViewModel.postValue(model)
    }
  }
}
