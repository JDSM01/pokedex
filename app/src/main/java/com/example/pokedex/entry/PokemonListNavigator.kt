package com.example.pokedex.entry

import androidx.fragment.app.FragmentManager
import com.example.pokedex.details.PokemonDetailsFragment
import com.example.pokedex.R
import com.example.pokedex.repositories.PokemonItem

class PokemonListNavigator(private val fragmentManager: FragmentManager) {

  fun navigateToDetails(pokemonItem: PokemonItem) {
    fragmentManager.beginTransaction()
        .replace(R.id.fragment_container, PokemonDetailsFragment.newInstance(pokemonItem))
        .addToBackStack(PokemonDetailsFragment::class.java.simpleName)
        .commit()
  }
}
