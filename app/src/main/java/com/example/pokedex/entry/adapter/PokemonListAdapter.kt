package com.example.pokedex.entry.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.example.pokedex.DiffUtilAdapter
import com.example.pokedex.R
import com.example.pokedex.repositories.PokemonItem

class PokemonListAdapter(private val action: (PokemonItem) -> Unit,
                         pokemonList: List<PokemonItem>) :
    DiffUtilAdapter<PokemonItem, PokemonListViewHolder>() {

  private var currentList: MutableList<PokemonItem> = pokemonList.toMutableList()

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonListViewHolder {
    val layout = if (viewType == ITEM_VIEW_TYPE) {
      LayoutInflater.from(parent.context)
          .inflate(R.layout.pokemon_item, parent, false)
    } else {
      LayoutInflater.from(parent.context)
          .inflate(R.layout.loading, parent, false)
    }
    return PokemonListViewHolder(layout, action)
  }

  override fun onBindViewHolder(holder: PokemonListViewHolder, position: Int) {
    holder.bind(currentList[position])
  }

  override fun getItemViewType(position: Int): Int {
    return if (currentList[position].isLoading) LOADING_VIEW_TYPE else ITEM_VIEW_TYPE
  }

  override fun getItemCount() = currentList.size

  fun getLastPokemonNumber(): Int {
    return if (currentList.isEmpty()) 0
    else currentList.last().number
  }

  fun setList(pokemonList: List<PokemonItem>) {
    applyDiffUtil(
        DiffRequest(pokemonList,
            PokemonListDiffCallback(ArrayList(currentList), ArrayList(pokemonList))))
  }

  fun addLoading() {
    val newList = ArrayList(currentList)
    newList.add(PokemonItem(true))
    applyDiffUtil(DiffRequest(newList, PokemonListDiffCallback(ArrayList(currentList), newList)))
  }

  companion object {
    private const val LOADING_VIEW_TYPE = 0
    private const val ITEM_VIEW_TYPE = 1
  }

  override fun dispatchUpdates(newItems: List<PokemonItem>, diffResult: DiffUtil.DiffResult) {
    currentList = newItems as MutableList<PokemonItem>
    diffResult.dispatchUpdatesTo(this)
  }
}
