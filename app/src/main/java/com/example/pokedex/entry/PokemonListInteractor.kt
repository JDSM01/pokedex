package com.example.pokedex.entry

import com.example.pokedex.repositories.PokemonListModel
import com.example.pokedex.repositories.PokemonRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PokemonListInteractor(private val repository: PokemonRepository) {

  suspend fun retrievePokemonList(offset: Int = 0): PokemonListModel {
    return withContext(Dispatchers.IO) {
      repository.retrieveList(offset)
    }
  }
}
