package com.example.pokedex.entry

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import com.example.pokedex.R
import com.example.pokedex.entry.adapter.PokemonListAdapter
import com.example.pokedex.repositories.PokemonItem
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_pokemon_list.*
import javax.inject.Inject

@AndroidEntryPoint
class PokemonListFragment : Fragment(), PokemonListView {

  @Inject
  lateinit var viewModel: PokemonListViewModel

  @Inject
  lateinit var navigator: PokemonListNavigator
  private lateinit var adapter: PokemonListAdapter

  companion object {
    @JvmStatic
    fun newInstance(): PokemonListFragment {
      return PokemonListFragment()
    }
  }

  override fun onCreateView(inflater: LayoutInflater,
                            container: ViewGroup?,
                            savedInstanceState: Bundle?): View? {
    return inflater.inflate(R.layout.fragment_pokemon_list, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    activity?.title = getString(R.string.app_name)
    adapter = PokemonListAdapter(provideOnItemClickAction(), emptyList())
    pokemon_recycler.adapter = adapter
    pokemon_recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
      override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        if (!recyclerView.canScrollVertically(1)) {
          viewModel.handleEndOfList(adapter.getLastPokemonNumber())
        }
      }
    })
    setupViewModel()
  }

  private fun setupViewModel() {
    setupOnPokemonList()
    setupOnLoadMore()
  }

  private fun provideOnItemClickAction(): (PokemonItem) -> Unit {
    return { pokemonItem: PokemonItem -> navigator.navigateToDetails(pokemonItem) }
  }

  override fun showList(pokemonList: List<PokemonItem>) {
    progress_bar.visibility = View.GONE
    adapter.setList(pokemonList)
  }

  private fun setupOnPokemonList() {
    viewModel.listViewModel
        .observe(viewLifecycleOwner) {
          if (it.error != null) showError()
          else showList(it.pokemonList)
        }
  }

  private fun setupOnLoadMore() {
    viewModel.showLoadMore.observe(viewLifecycleOwner) { if (it) adapter.addLoading() }
  }

  override fun showError() {
    Toast.makeText(context, R.string.error_info, Toast.LENGTH_LONG)
        .show()
  }
}
