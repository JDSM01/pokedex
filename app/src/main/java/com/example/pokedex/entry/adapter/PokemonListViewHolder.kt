package com.example.pokedex.entry.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pokedex.repositories.PokemonItem
import kotlinx.android.synthetic.main.pokemon_item.view.*

class PokemonListViewHolder(itemView: View, private val action: (PokemonItem) -> Unit) :
    RecyclerView.ViewHolder(itemView) {

  fun bind(pokemonItem: PokemonItem) {
    if (!pokemonItem.isLoading) {
      itemView.pokemon_name.text = pokemonItem.name
      itemView.pokemon_number.text = pokemonItem.number.toString()
      Glide.with(itemView.context)
          .load(pokemonItem.image)
          .circleCrop()
          .into(itemView.pokemon_image)
      itemView.setOnClickListener { action.invoke(pokemonItem) }
    }
  }
}
