package com.example.pokedex.entry

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pokedex.repositories.PokemonRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
class PokemonListModule {

  @Provides
  fun providesPokemonViewModel(fragment: Fragment,
                               factory: PokemonListViewModelFactory): PokemonListViewModel {
    return ViewModelProvider(fragment, factory).get(PokemonListViewModel::class.java)
  }

  @Provides
  fun providesPokemonListInteractor(pokemonRepository: PokemonRepository): PokemonListInteractor {
    return PokemonListInteractor(pokemonRepository)
  }

  @Provides
  fun providesPokemonListNavigator(fragment: Fragment): PokemonListNavigator {
    return PokemonListNavigator(fragment.parentFragmentManager)
  }

  @Provides
  fun providesPokemonListViewModelFactory(
      interactor: PokemonListInteractor): PokemonListViewModelFactory {
    return PokemonListViewModelFactory(interactor)
  }
}

class PokemonListViewModelFactory(private val interactor: PokemonListInteractor) :
    ViewModelProvider.Factory {
  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(PokemonListViewModel::class.java)) {
      return PokemonListViewModel(interactor) as T
    }
    throw IllegalArgumentException("Unknown ViewModel class")
  }
}