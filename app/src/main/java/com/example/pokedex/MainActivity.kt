package com.example.pokedex

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.pokedex.entry.PokemonListFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    if (savedInstanceState == null) showListFragment()
  }

  private fun showListFragment() {
    supportFragmentManager.beginTransaction()
        .add(R.id.fragment_container, PokemonListFragment.newInstance())
        .commit()
  }
}