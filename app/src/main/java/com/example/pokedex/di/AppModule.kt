package com.example.pokedex.di

import android.content.Context
import com.example.pokedex.LogInterceptor
import com.example.pokedex.MyCacheInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class AppModule {

  @Provides
  fun providesCache(@ApplicationContext context: Context): Cache {
    val httpCacheDirectory = File(context.cacheDir, "http-cache")
    val cacheSize = 10 * 1024 * 1024 // 10 MiB

    return Cache(httpCacheDirectory, cacheSize.toLong())
  }

  @Singleton
  @Provides
  fun providesOkHttpClient(cache: Cache): OkHttpClient {

    return OkHttpClient.Builder()
        .addInterceptor(LogInterceptor())
        .addInterceptor(MyCacheInterceptor())
        .cache(cache)
        .connectTimeout(15, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)
        .build()
  }
}