package com.example.pokedex.di

import com.example.pokedex.repositories.PokemonApi
import com.example.pokedex.repositories.PokemonRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RepositoryModule {

  @Singleton
  @Provides
  fun providesPokemonRepository(api: PokemonApi): PokemonRepository {
    return PokemonRepository(api)
  }
}