package com.example.pokedex

import com.example.pokedex.repositories.*
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PokemonRepositoryTest {

  @Mock
  lateinit var pokemonApi: PokemonApi
  private lateinit var pokemonListResponse: PokemonListResponse
  private lateinit var pokemonDetailsResponse: PokemonDetailsResponse

  @Before
  fun setup() {
    pokemonListResponse =
        PokemonListResponse(0, null, null,
            listOf(PokemonResponse("name1", "url"), PokemonResponse("name2", "url")))
    val pokemonSecondListResponse = PokemonListResponse(0, null, null,
        listOf(PokemonResponse("name21", "url"), PokemonResponse("name22", "url")))
    pokemonDetailsResponse = PokemonDetailsResponse(emptyList(), "20", 20, emptyList(), 10)
    Mockito.`when`(pokemonApi.retrieveList(0))
        .thenReturn(Single.just(pokemonListResponse))
    Mockito.`when`(pokemonApi.retrieveList(20))
        .thenReturn(Single.just(pokemonSecondListResponse))
    Mockito.`when`(pokemonApi.retrievePokemon(20))
        .thenReturn(Single.just(pokemonDetailsResponse))
  }


  @Test
  fun retrievePokemonListTest() {
    val observer = TestObserver<PokemonListModel>()
    val repository = PokemonRepository(pokemonApi)
    repository.retrieveList(0)
        .subscribe(observer)
    observer.assertNoErrors()
        .assertValue {
          it == PokemonListModel(listOf(
              PokemonItem(1, "name1",
              "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png"),
              PokemonItem(2, "name2",
                  "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/2.png")
          ))
        }
  }

  @Test
  fun retrieveSecondPokemonListTest() {
    val observer = TestObserver<PokemonListModel>()
    val repository = PokemonRepository(pokemonApi)
    repository.retrieveList(20)
        .subscribe(observer)
    observer.assertNoErrors()
        .assertValue {
          it == PokemonListModel(listOf(
              PokemonItem(21, "name21",
              "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/21.png"),
              PokemonItem(22, "name22",
                  "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/22.png")
          ))
        }
  }

  @Test
  fun retrievePokemonDetailTest() {
    val observer = TestObserver<PokemonModel>()
    val repository = PokemonRepository(pokemonApi)
    repository.retrievePokemon(20)
        .subscribe(observer)
    observer.assertNoErrors()
        .assertValue {
          it == PokemonModel(emptyList(), "20", 20, 10, emptyList())
        }
  }
}